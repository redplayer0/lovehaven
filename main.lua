-- main.lua

-- camera = require("lib.hump.camera").new()

local gamestate = require("lib.hump.gamestate")

local main_menu = require("states.main_menu")

function love.load()
  love.mouse.setVisible(false)
  gamestate.registerEvents()
  gamestate.switch(main_menu)
end
