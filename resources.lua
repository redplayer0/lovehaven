local hex = require("hex")

local M = {}

-- UI related
M.font_size = 16

-- Hex related resources
M.hex_size = 42
M.layout = hex.layout(hex.layout_pointy, hex.point(M.hex_size, M.hex_size), hex.point(0, 0))
M.active_hex = hex.hex(0, 0, 0)

-- Input related to navigated more easily a hex grid
M.last_key = "right"

M.chars_placed = false
M.grid = {}
M.chapter_data = {}
M.enemy_type_numbers = {}
M.has_enemy_picked_card = {}
M.has_selected_enemy_cards = false

M.active_character = nil -- entity id


M.selection = nil -- list of hexes or ids

return M
