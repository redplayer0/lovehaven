local gamestate = require("lib.hump.gamestate")
local start_game = require("states.start_game")

local main_menu = {}

function main_menu:init()
  love.graphics.setBackgroundColor(0, 0, 0)
  love.graphics.setColor(1, 1, 1)
  love.graphics.setDefaultFilter("nearest", "nearest")
  -- love.graphics.setNewFont("assets/gloomfont.ttf", 32)
  love.graphics.setNewFont("assets/jet.ttf", 32)
  self.options = {
    {"Start Game", function() return gamestate.push(start_game) end},
    {"Load Game", function() end},
    {"Quit", love.event.quit},
  }
  self.current = 0
end

function main_menu:enter()
  self.current = 0
end

function main_menu:keypressed(key)
  if key == "down" then
    self.current = self.current + 1
  elseif key == "up" then
    self.current = self.current - 1
  elseif key == "return" then
    return self.options[self.current%#self.options + 1][2]()
  end
end

function main_menu:draw()
  for i, opt in ipairs(self.options) do
    if i == self.current%#self.options + 1 then
      love.graphics.setColor(1, 1, 0)
      love.graphics.print(opt[1], 0, i*64)
      love.graphics.setColor(1, 1, 1)
    else
      love.graphics.print(opt[1], 0, i*64)
    end
  end
end

return main_menu
