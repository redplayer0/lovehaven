local entities = require("entities")
local chapters = require("chapters")
local draw = require("draw")
local gamestate = require("lib.hump.gamestate")
local hex = require("hex")
local utils = require("utils")
local resources = require("resources")
local select_cards = require("states.select_cards")
local pretty = require("lib.batteries.pretty")

local select_hexes = {}

function select_hexes:init()
end

function select_hexes:enter(_, total, range, enemy)
  self.enemy = enemy or false
  self.total = total
  self.range = range
  resources.selection = {}
end

function select_hexes:update()
  self.active_hex = utils.activeHex(resources.layout)
  -- move action -> selection is hexes
  if #self.selection == self.total and not enemy then
    coroutine.resume(resources.current_action, self.selection)
  end
  -- attack action -> selection is entities
  if #self.selection == self.total and enemy then
    local ents = {}
    for _, h in ipairs(self.selection) do
      -- get entities
      for id, e in ipairs(entities) do
        if hex.compare_hex(e.position, h) then
          table.insert(ents, id)
        end
      end
      self.selection = ents
    end
    coroutine.resume(resources.current_action, self.selection)
  end
end

function select_hexes:draw()
  draw.draw_grid(self.grid, resources.layout)
  draw.objects()
  draw.enemies()
  draw.allies()
  love.graphics.print(hex.stringify_double(self.active_hex), 0, 32)
  draw.draw_hex(self.active_hex, resources.layout, 0, 0, 1)
end

function select_hexes:mousepressed(_, _, btn)
  if btn == 1 then
    table.insert(self.selection, self.active_hex)
  end
end

function select_hexes:mousemoved(x, y , dx, dy)
  if love.mouse.isDown(1) and love.mouse.isDown(2) then
    local origin_x = resources.layout.origin.x
    local origin_y = resources.layout.origin.y
    local size = resources.layout.size
    local orientation = resources.layout.orientation
    origin_x = origin_x + dx*2
    origin_y = origin_y + dy*2
    resources.layout = hex.layout(orientation, size, hex.point(origin_x, origin_y))
  end
end

return select_hexes
