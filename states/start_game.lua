local gamestate = require("lib.hump.gamestate")
local campaign = require("states.campaign")

local start_game = {}

function start_game:init()
  self.options = {
    {"Campaign", function() return gamestate.push(campaign) end},
    {"Characters", function() end},
    {"Shop", function() end},
    {"Back", function() return gamestate.pop() end},
  }
  self.current = 0
end

function start_game:enter(from)
  self.from = from
end

function start_game:keypressed(key)
  if key == "down" then
    self.current = self.current + 1
  elseif key == "up" then
    self.current = self.current - 1
  elseif key == "backspace" or key == "escape" then
    return gamestate.switch(self.from)
  elseif key == "return" then
    return self.options[self.current%#self.options + 1][2]()
  end
end

function start_game:draw()
  self.from.draw(self)
end

return start_game
