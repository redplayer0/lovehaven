local draw = require("draw")
local engine = require("engine")
local gamestate = require("lib.hump.gamestate")
local hex = require("hex")
local resources = require("resources")
local entities = require("entities")
local select_cards = require("states.select_cards")
local pretty = require("lib.batteries.pretty")
local turn_handler = require("states.turn_handler")

local choose_actions = {}

function choose_actions:init()
  self.title = "Select actions for every character"
end

function choose_actions:enter()
  self.title = "Select actions for every character"
  for _, e in ipairs(entities) do
    if e.ingame then
      e.initiative = nil
    end
  end
end

function choose_actions:resume()
  if engine.has_everyone_selected() then
    self.title = "Press <enter> to continue"
  else
    self.title = "Select actions for every character"
  end
end

function choose_actions:update()
end

function choose_actions:draw()
  draw.all()
  love.graphics.print(hex.stringify_double(resources.active_hex), 0, resources.font_size*1.5)
  love.graphics.print(self.title, 0, 0)
end

function choose_actions:keypressed(key)
  engine.active_hex(key)
  if key == "return" or key == "a" then
    if engine.has_everyone_selected() then
      -- TODO
      resources.has_selected_enemy_cards = false
      engine.pick_enemy_cards()
      gamestate.switch(turn_handler)
    else
      resources.active_character = engine.get_character()
      if resources.active_character then
        gamestate.push(select_cards, resources.active_character, 2)
      end
    end
  elseif key == "d" then
    resources.active_character = engine.get_character()
    if resources.active_character then
      gamestate.push(select_cards, resources.active_character, 0)
    end
  end
end

return choose_actions
