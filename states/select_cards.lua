local gamestate = require("lib.hump.gamestate")
local entities = require("entities")
local resources = require("resources")
local abilities = require("abilities")

local select_cards = {}

function select_cards:init()
end

function select_cards:enter(_, entity_id, total)
  love.graphics.setNewFont("assets/jet.ttf", resources.font_size + 6)
  self.entity_id = entity_id
  self.total = total
  self.selected = 0
  self.lead = false
  local class = entities[entity_id].class
  self.cards = {}
  for name, e in pairs(abilities[class]) do
    if not e.discarded and not e.lost then
      e.selected = false
      table.insert(
        self.cards,
        {
          initiative = e.initiative,
          name = name,
          id = #self.cards+1,
          selected = false,
          lead = false,
          -- desc = e.description,
        }
      )
    end
  end
  self.current = 0
  if total == 0 then
    entities[entity_id].initiative = nil
    gamestate.pop()
  end
end

function select_cards:leave()
  love.graphics.setNewFont("assets/jet.ttf", resources.font_size)
end

function select_cards:update(dt)
  local t = 0
  for _, c in ipairs(self.cards) do
    if c.selected then
      t = t + 1
    end
  end
  self.selected = t
end

function select_cards:draw(dt)
  local pad = resources.font_size * 2
  local y = 100
  for i, card in ipairs(self.cards) do
    local text
    if card.initiative < 10 then
      text = card.initiative.."     "..card.name
    else
      text = card.initiative.."    "..card.name
    end
    if card.selected then
      text = ">>  "..text
    end
    if card.lead then
      text = ">>"..text
    end
    if i == self.current%#self.cards + 1 then
      love.graphics.setColor(1, 1, 0)
      love.graphics.print(text, 0, y)
      love.graphics.setColor(1, 1, 1)
    else
      love.graphics.print(text, 0, y)
    end
    -- love.graphics.print(self.cards[i].desc, 740, resources.font_size)
    y = y + pad
  end
  if self.selected ~= 2 then
    love.graphics.print("Select 2 cards", 0, 0)
  elseif self.selected == 2 and self.lead then
    love.graphics.print("Press enter to continue", 0, 0)
  elseif self.selected == 2 and not self.lead then
    love.graphics.print("Select one of the selected cards to lead", 0, 0)
  end
end

function select_cards:keypressed(key)
  local current_card = self.cards[self.current%#self.cards+1]
  local done = self.selected == 2
  if key == "down" then
    self.current = self.current + 1
  elseif key == "up" then
    self.current = self.current - 1
  elseif key == "right" then
    if not done and current_card.selected then
      if not self.lead then
        current_card.lead = true
        self.lead = true
        self.initiative = current_card.initiative
      end
    elseif not done and not current_card.selected then
      current_card.selected = true
    elseif done and current_card.selected and not current_card.lead and not self.lead then
      current_card.lead = true
      self.lead = true
      self.initiative = current_card.initiative
    end
  elseif key == "left" and current_card.selected then
    current_card.selected = false
    if current_card.lead then
      current_card.lead = false
      self.lead = false
      self.initiative = 0
    end
  elseif key == "left" and not current_card.selected then
    gamestate.pop()
  elseif key == "return" and done and self.lead then
    entities[self.entity_id].initiative = self.initiative
    for _, c in ipairs(self.cards) do
      if c.selected then
        entities[c.id].selected = true
      end
    end
    gamestate.pop()
  end
end

return select_cards
