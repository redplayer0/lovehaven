local entities = require("entities")
local chapters = require("chapters")
local draw = require("draw")
local gamestate = require("lib.hump.gamestate")
local hex = require("hex")
local utils = require("utils")
local resources = require("resources")
local engine = require("engine")
local select_cards = require("states.select_cards")
local pretty = require("lib.batteries.pretty")

local turn_handler = {}

function turn_handler:init()
end

function turn_handler:enter()
  resources.active_character = engine.find_lowest_initiative_char()
end

function turn_handler:draw()
  draw.all()
  draw.active_character()
end

function turn_handler:keypressed(key)
  engine.active_hex(key)
end


return turn_handler
