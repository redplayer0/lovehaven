local gamestate = require("lib.hump.gamestate")
local init_chapter = require("states.init_chapter")

local campaign = {}

function campaign:init(saveFile)
  saveFile = saveFile or nil
  self.current = 0
  if saveFile then
    -- TODO
  else
    self.chapters = {1, 2, 3, 6}
  end
end

function campaign:enter(from)
  self.from = from
end

function campaign:keypressed(key)
  if key == "down" then
    self.current = self.current + 1
  elseif key == "up" then
    self.current = self.current - 1
  elseif key == "backspace" or key == "escape" then
    return gamestate.pop()
  elseif key == "return" then
    local chapter = self.current%#self.chapters + 1
    return gamestate.switch(init_chapter, chapter)
  end
end

function campaign:draw()
  for i, ch in ipairs(self.chapters) do
    if i == self.current%#self.chapters + 1 then
      love.graphics.setColor(1, 1, 0)
      love.graphics.print(ch, 0, i*64)
      love.graphics.setColor(1, 1, 1)
    else
      love.graphics.print(ch, 0, i*64)
    end
  end
end

return campaign
