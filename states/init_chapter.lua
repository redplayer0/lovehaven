local chapters = require("chapters")
local draw = require("draw")
local engine = require("engine")
local gamestate = require("lib.hump.gamestate")
local hex = require("hex")
local resources = require("resources")
local entities = require("entities")
local choose_actions = require("states.choose_actions")
local pretty = require("lib.batteries.pretty")


local init_chapter = {}

function init_chapter:init()
  love.graphics.setLineStyle("smooth")
  love.graphics.setLineWidth(1)
  love.graphics.setNewFont("assets/jet.ttf", resources.font_size)
  self.title = "Assign positions to your characters"
end

function init_chapter:enter(_, chapter)
  love.graphics.setNewFont("assets/jet.ttf", resources.font_size)
  if chapter then
    resources.chapter_data = engine.loadChapter(chapters[chapter])
    self.chapter_level = 0
    -- load first room
    engine.load_room(resources.chapter_data, resources.chapter_data.first_room, resources.grid)
    self.chars = {}
    for i, e in ipairs(entities) do
      if e.player and e.ingame then
        table.insert(self.chars, i)
      end
    end
  end
end

function init_chapter:update()
end

function init_chapter:draw()
  draw.all()
  love.graphics.print(hex.stringify_double(resources.active_hex), 0, resources.font_size*1.5)
  if #self.chars == 0 and not resources.chars_placed then
    love.graphics.print("Press <ENTER> to start playing", 0, love.graphics.getHeight() - resources.font_size*1.5)
  end
  love.graphics.print(self.title, 0, 0)
end


function init_chapter:keypressed(key)
  engine.active_hex(key)
  if resources.chars_placed then
    if key == "return" or key == "a" then
      for i, e in ipairs(entities) do
        if e.entry then
          entities[i] = {}
        end
      end
      gamestate.switch(choose_actions)
    end
    resources.chars_placed = true
  end
  engine.place_characters(key, self.chars)
end


-- function init_chapter:mousemoved(x, y , dx, dy)
--   if love.mouse.isDown(1) and love.mouse.isDown(2) then
--     local origin_x = resources.layout.origin.x
--     local origin_y = resources.layout.origin.y
--     local size = resources.layout.size
--     local orientation = resources.layout.orientation
--     origin_x = origin_x + dx*2
--     origin_y = origin_y + dy*2
--     resources.layout = hex.layout(orientation, size, hex.point(origin_x, origin_y))
--   end
-- end

return init_chapter
