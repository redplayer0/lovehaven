-- Ability card system
local abilities = {}

-- Card template example table
-- table for all cards of class rogue
abilities.rogue = {
  -- example of a card
  ["Throwing Knives"] = {
    initiative = 10, -- must have an initiative field
    -- top/bottom are tables that contain the text of the cards
    -- eg.
    description = "top\n attack 2\n range 3\n targets 2\n Gain 1 exp\nbottom\n loot 2\n loss X",
    top = {
      attack = {damage = 2, range = 3, targets = 2},
      exp = 1,
    },
    bottom = {
      loot = 2,
      loss = true,
    }
  },
  ["Single out"] = {
    initiative = 86,
    top = {
      special = {"attack_if_adj_allies", 3, 2, 1}
    },
    bottom = {
      special = "single_out_buff"
    }
  }
}

abilities.brute = {
  ["Overwhelming Assault"] = {
    initiative = 61,
    description = "top\n attack 6\n Gain 2 exp\n loss X\n bottom\n move 3\n push 2",
    top = {
      attack = {damage = 6},
      exp = 2,
      loos = true
    },
    bottom = {
      move = 3,
      push = {distance = 2}
    }
  },
  ["Spare Dagger"] = {
    initiative = 27,
    top = {
      attack = {damage = 3, range = 3},
      exp = 1,
    },
    bottom = {
      attack = {damage = 3}
    }
  },
}

return abilities





