local hex = {}

local points = {}

function hex.point(x, y)
  if points[x] and points[x][y] then return points[x][y] end
  points[x] = {}
  points[x][y] = {x = x, y = y}
  return points[x][y]
end

function hex.compare_points(a, b)
  return a.x == b.x and a.y == b.y
end

local hexes = {}

function hex.hex(q, r, s)
  -- print(q..', '..r..', '..s)
  if hexes[q] and hexes[q][r] and hexes[q][r][s] then return hexes[q][r][s] end
  assert(not(math.floor(0.5 + q + r + s) ~= 0), "q + r + s must be 0")
  hexes[q] = {}
  hexes[q][r] = {}
  hexes[q][r][s] = {q = q, r = r, s = s}
  return hexes[q][r][s]
end

function hex.stringify_cube(h)
  return h.q..","..h.r..","..h.s
end

function hex.stringify_double(h)
  local double = hex.rdoubled_from_cube(h)
  return double.col..","..double.row
end

function hex.compare_hex(a, b)
  return a.q == b.q and a.r == b.r and a.s == b.s
end

function hex.add(a, b)
  return hex.hex(a.q + b.q, a.r + b.r, a.s + b.s)
end

function hex.subtract(a, b)
  return hex.hex(a.q - b.q, a.r - b.r, a.s - b.s)
end

function hex.scale(a, k)
  return hex.hex(a.q * k, a.r * k, a.s * k)
end

function hex.rotate_left(a)
  return hex.hex(-a.s, -a.q, -a.r)
end

function hex.rotate_right(a)
  return hex.hex(-a.r, -a.s, -a.q)
end

hex.directions = {hex.hex(1, 0, -1), hex.hex(1, -1, 0), hex.hex(0, -1, 1), hex.hex(-1, 0, 1), hex.hex(-1, 1, 0), hex.hex(0, 1, -1)}

function hex.direction(direction)
  return hex.directions[1+direction]
end

function hex.neighbor(h, direction)
  return hex.add(h, hex.direction(direction))
end

hex.diagonals = {hex.hex(2, -1, -1), hex.hex(1, -2, 1), hex.hex(-1, -1, 2), hex.hex(-2, 1, 1), hex.hex(-1, 2, -1), hex.hex(1, 1, -2)}
function hex.diagonal_neighbor(h, direction)
  return hex.add(h, hex.diagonals[1+direction])
end

function hex.length(h)
  return math.floor((math.abs(h.q) + math.abs(h.r) + math.abs(h.s)) / 2)
end

function hex.distance(a, b)
  return hex.length(hex.subtract(a, b))
end

function hex.round(h)
  local qi = math.floor(math.floor(0.5 + h.q))
  local ri = math.floor(math.floor(0.5 + h.r))
  local si = math.floor(math.floor(0.5 + h.s))
  local q_diff = math.abs(qi - h.q)
  local r_diff = math.abs(ri - h.r)
  local s_diff = math.abs(si - h.s)
  if q_diff > r_diff and q_diff > s_diff then
    qi = -ri - si
  else
    if r_diff > s_diff then
    ri = -qi - si
    else
    si = -qi - ri
    end
  end
  return hex.hex(qi, ri, si)
end

function hex.lerp(a, b, t)
  return hex.hex(a.q *(1.0 - t) + b.q * t, a.r *(1.0 - t) + b.r * t, a.s *(1.0 - t) + b.s * t)
end

function hex.linedraw(a, b)
  local N = hex.distance(a, b)
  local a_nudge = hex.hex(a.q + 1e-06, a.r + 1e-06, a.s - 2e-06)
  local b_nudge = hex.hex(b.q + 1e-06, b.r + 1e-06, b.s - 2e-06)
  local results = {}
  local step = 1.0 / math.max(N, 1)
  for i = 0, N do
    table.insert(results, hex.round(hex.lerp(a_nudge, b_nudge, step * i)))
  end
  return results
end


function hex.offsetCoord(col, row)
  return {col = col, row = row}
end

EVEN = 1
ODD = -1
function hex.qoffset_from_cube(offset, h)
  local col = h.q
  local row = h.r + math.floor(h.q + offset *(h.q and 1) / 2)
  if offset ~= EVEN and offset ~= ODD then
    error("offset must be EVEN(+1) or ODD(-1)")
  end
  return hex.offsetCoord(col, row)
end

function hex.qoffset_to_cube(offset, h)
  local q = h.col
  local r = h.row - math.floor((h.col + offset *(h.col and 1) / 2))
  local s = -q - r
  if offset ~= EVEN and offset ~= ODD then
    error("offset must be EVEN(+1) or ODD(-1)")
  end
  return hex.hex(q, r, s)
end

function hex.roffset_from_cube(offset, h)
  local col = h.q + math.floor((h.r + offset *(h.r and 1) / 2))
  local row = h.r
  if offset ~= EVEN and offset ~= ODD then
    error("offset must be EVEN(+1) or ODD(-1)")
  end
  return hex.offsetCoord(col, row)
end

function hex.roffset_to_cube(offset, h)
  local q = h.col - math.floor((h.row + offset *(h.row and 1) / 2))
  local r = h.row
  local s = -q - r
  if offset ~= EVEN and offset ~= ODD then
    error("offset must be EVEN(+1) or ODD(-1)")
  end
  return hex.hex(q, r, s)
end


function hex.doubledCoord(col, row)
  return {col = col, row = row}
end

function hex.qdoubled_from_cube(h)
  local col = h.q
  local row = 2 * h.r + h.q
  return hex.doubledCoord(col, row)
end

function hex.qdoubled_to_cube(h)
  local q = h.col
  local r = math.floor((h.row - h.col) / 2)
  local s = -q - r
  return hex.hex(q, r, s)
end

function hex.rdoubled_from_cube(h)
  local col = 2 * h.q + h.r
  local row = h.r
  return hex.doubledCoord(col, row)
end

function hex.rdoubled_to_cube(h)
  local q = math.floor((h.col - h.row) / 2)
  local r = h.row
  local s = -q - r
  return hex.hex(q, r, s)
end


function hex.oreintation(f0, f1, f2, f3, b0, b1, b2, b3, start_angle)
  return {f0 = f0, f1 = f1, f2 = f2, f3 = f3, b0 = b0, b1 = b1, b2 = b2, b3 = b3, start_angle = start_angle}
end

function hex.layout(orientation, size, origin)
  return {orientation = orientation, size = size, origin = origin}
end

hex.layout_pointy = hex.oreintation(math.sqrt(3.0), math.sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0, math.sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0, 0.5)
hex.layout_flat = hex.oreintation(3.0 / 2.0, 0.0, math.sqrt(3.0) / 2.0, math.sqrt(3.0), 2.0 / 3.0, 0.0, -1.0 / 3.0, math.sqrt(3.0) / 3.0, 0.0)


function hex.to_pixel(layout, h)
  local M = layout.orientation
  local size = layout.size
  local origin = layout.origin
  local x =(M.f0 * h.q + M.f1 * h.r) * size.x
  local y =(M.f2 * h.q + M.f3 * h.r) * size.y
  return hex.point(x + origin.x, y + origin.y)
end

function hex.from_pixel(layout, p)
  local M = layout.orientation
  local size = layout.size
  local origin = layout.origin
  local pt = hex.point((p.x - origin.x) / size.x,(p.y - origin.y) / size.y)
  local q = M.b0 * pt.x + M.b1 * pt.y
  local r = M.b2 * pt.x + M.b3 * pt.y
  return hex.hex(q, r, -q - r)
end

function hex.corner_offset(layout, corner)
  local M = layout.orientation
  local size = layout.size
  local angle = 2.0 * math.pi *(M.start_angle - corner) / 6.0
  return hex.point(size.x * math.cos(angle), size.y * math.sin(angle))
end

function hex.corners(layout, h)
  local corners = {}
  local center = hex.to_pixel(layout, h)
  for i = 0, 5 do
    local offset = hex.corner_offset(layout, i)
    table.insert(corners, hex.point(center.x + offset.x, center.y + offset.y))
  end
  return corners
end

return hex
