local entities = {
  {
    player = true,
    class = 'rogue',
    unlocked = true,
    ingame = true,
    stamina = 9,
    level = 1,
    exp = 0,
    max_hp = {8, 9, 11, 12, 14, 15, 17, 18, 20},
  },
  {
    player = true,
    class = 'brute',
    unlocked = true,
    ingame = true,
    stamina = 11,
  },
}

return entities

