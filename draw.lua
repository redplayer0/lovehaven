local hex = require("hex")
local resources = require("resources")
local entities = require("entities")

local draw = {}

function draw.draw_hex(h, layout, ...)
  local corners = hex.corners(layout, h)
  local polygon_points = {}
  for _, corner in ipairs(corners) do
    table.insert(polygon_points, corner.x)
    table.insert(polygon_points, corner.y)
  end
  if ... then
    love.graphics.setColor(...)
    love.graphics.polygon("line", unpack(polygon_points))
    love.graphics.setColor(1, 1, 1)
  else
    love.graphics.polygon("line", unpack(polygon_points))
  end
end

function draw.draw_grid(grid, layout, ...)
  for _, h in pairs(grid) do
    draw.draw_hex(h, layout, ...)
  end
end

function draw.allies()
  local full = resources.hex_size
  local half = resources.hex_size/2
  for _, e in ipairs(entities) do
    if e.player and e.position then
      local pos = hex.to_pixel(resources.layout, e.position)
      love.graphics.print(e.class, pos.x - half, pos.y - full/1.5)
      if e.initiative then
        love.graphics.setColor(1, 0, 0)
        love.graphics.print(e.initiative, pos.x - half/2, pos.y - half/2)
        love.graphics.setColor(1, 1, 1)
      end
    end
  end
end

function draw.enemies()
  local full = resources.hex_size
  local half = resources.hex_size/2
  for _, e in ipairs(entities) do
    if e.position and e.is_enemy then
      local pos = hex.to_pixel(resources.layout, e.position)
      love.graphics.print(e.number.." "..e.icon, pos.x - half, pos.y - full/1.5)
      if e.initiative then
        love.graphics.setColor(1, 0, 0)
        love.graphics.print(e.initiative, pos.x - half/2, pos.y - half/2)
        love.graphics.setColor(1, 1, 1)
      end
    end
  end
end

function draw.objects()
  local full = resources.hex_size
  local half = resources.hex_size/2
  for _, e in ipairs(entities) do
    if e.object then
      local pos = hex.to_pixel(resources.layout, e.position)
      love.graphics.print(e.icon, pos.x - half, pos.y - half/2)
    end
  end
end

function draw.active_hex()
  draw.draw_hex(resources.active_hex, resources.layout, 0, 0, 1)
end

function draw.all()
  draw.draw_grid(resources.grid, resources.layout)
  draw.objects()
  draw.enemies()
  draw.allies()
  draw.active_hex()
end

function draw.active_character()
  if resources.active_character then
    draw.draw_hex(entities[resources.active_character].position, resources.layout, 1, 1, 0)
  end
end

return draw
