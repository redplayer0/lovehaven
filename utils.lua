local utils = {}

function utils.deepcopy(t)
  local u = {}
  for k, v in pairs(t) do u[k] = v end
  return setmetatable(u, getmetatable(t))
end

function utils.createGridCanvas(grid, layout)
  local gridCanvas = love.graphics.newCanvas(2080, 2080)
  love.graphics.setLineStyle("smooth")
  love.graphics.setLineWidth(2)
  love.graphics.setCanvas(gridCanvas)
  for _, h in pairs(grid) do
    utils.draw_hex(h, layout)
  end
  love.graphics.setCanvas()
  return gridCanvas
end

return utils
