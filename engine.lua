local entities = require("entities")
local enemies = require("enemies")
-- local gspot = require("lib.gspot")
local hex = require("hex")
local utils = require("utils")
local pretty = require("lib.batteries.pretty")
local resources = require("resources")
local enemy_abilities = require("enemy_abilities")

local engine = {}

-- @chapter: an item in the chapters table
function engine.loadChapter(chapter)
  local chapter_data = {}
  chapter_data.enemy_defs = chapter.enemy_defs
  chapter_data.first_room = chapter.first_room
  for _, enemy_type in pairs(chapter_data.enemy_defs) do
    resources.enemy_type_numbers[enemy_type] = {1,2,3,4,5,6,7,8,9,10}
  end
  local grid = chapter.grid
  local y = 0
  -- split grid in rows
  for line in grid:gmatch("([^\n]*)\n?") do
    for x = 1, #line, 2 do
      -- get every char in the line
      local char = line:sub(x, x)
      if char ~= " " then
        local tile = {}
        tile.y = y
        tile.x = x/2+2
        tile.room = line:sub(x, x)
        tile.obj = line:sub(x+1, x+1)
        table.insert(chapter_data, tile)
      end
    end
    -- print(y.."     "..line)
    -- inc y for next line
    y = y + 1
  end
  return chapter_data
end

function engine.load_room(chapter_data, room, grid)
  for _, tile in ipairs(chapter_data) do
    if tile.room == room then
      local pos = hex.rdoubled_to_cube(hex.doubledCoord(tile.x, tile.y))
      table.insert(grid, pos)
      if tile.obj ~= "." then
        local icon = tile.obj
        -- checks if icon is defined as enemy definition
        -- use lower case because definitions use only lower case chapters
        -- upper case simply shoes that it is an elite monster
        if chapter_data.enemy_defs[string.lower(icon)] then
          local enemy_type = chapter_data.enemy_defs[string.lower(icon)]
          -- TODO use chapter level here
          local monster_level = 0
          local subclass = "normal"
          if icon == string.upper(icon) then
            subclass = "elite"
          end
          local enemy_entity = utils.deepcopy(enemies.monsters[enemy_type].level[monster_level+1][subclass])
          if subclass == "elite" then
            enemy_entity.elite = true
          end
          enemy_entity.position = pos
          enemy_entity.icon = icon
          enemy_entity.is_enemy = true
          enemy_entity.is_alive = true
          enemy_entity.enemy_type = enemy_type
          enemy_entity.number = table.remove(resources.enemy_type_numbers[enemy_type], love.math.random(#resources.enemy_type_numbers[enemy_type]))
          table.insert(entities, enemy_entity)
        end
        if icon == "2" then
          table.insert(entities, {object = true, door = true, is_open = false, position = pos, icon = "door"})
        end
        if icon == "e" then
          table.insert(entities, {object = true, entry = true, is_empty = true, position = pos, icon = "entry"})
        end
        if icon == "E" then
          table.insert(entities, {object = true, entry = true, is_empty = true, position = pos, icon = "entry"})
          resources.active_hex = pos
        end
      end
    end
  end
  -- pretty.print(entities)
  print("loaded entities")
end

function engine.place_characters(key, chars)
  if key == "a" or key == "return" then
    for _, e in ipairs(entities) do
      -- check is an entry's position matches the clicked hex
      if e.entry and hex.compare_hex(e.position, resources.active_hex) then
        if e.is_empty then
          local char = table.remove(chars, 1)
          entities[char].position = e.position
          e.is_empty = false
          if #chars == 0 then
            resources.chars_placed = true
          end
        end
      end
    end
  end
  if key == "d" then
    local active_hex = resources.active_hex
    for i, e in ipairs(entities) do
      if e.entry and not e.is_empty and hex.compare_hex(e.position, active_hex) then
        local e_pos = e.position
        local e_id = i
        print('got here')
        for i, e in ipairs(entities) do
          if e.player and e.position and hex.compare_hex(e.position, e_pos) then
            table.insert(chars, i)
            entities[i].position = nil
            entities[e_id].is_empty = true
            resources.chars_placed = false
          end
        end
      end
    end
  end
end

function engine.activeHex(layout)
  local x, y = camera:mousePosition()
  return hex.round(hex.from_pixel(layout, hex.point(x, y)))
end

function engine.active_hex(key)
  if key == "up" then
    if resources.last_key == "right" then
      resources.active_hex = hex.add(resources.active_hex, hex.directions[2])
    else
      resources.active_hex = hex.add(resources.active_hex, hex.directions[3])
    end
  end
  if key == "down" then
    if resources.last_key == "right" then
      resources.active_hex = hex.add(resources.active_hex, hex.directions[6])
    else
      resources.active_hex = hex.add(resources.active_hex, hex.directions[5])
    end
  end
  if key == "right" then
    resources.last_key = key
    resources.active_hex = hex.add(resources.active_hex, hex.directions[1])
  end
  if key == "left" then
    resources.last_key = key
    resources.active_hex = hex.add(resources.active_hex, hex.directions[4])
  end
end

function engine.characters_without_initiative()
  local num = 0
  for _, e in ipairs(entities) do
    if e.ingame and #e.selected_cards ~= 2 then
      num = num + 1
    end
  end
  return num
end

function engine.get_character()
  for i, e in ipairs(entities) do
    if e.ingame and hex.compare_hex(e.position, resources.active_hex) then
      return i
    end
  end
end

function engine.has_everyone_selected()
  for _, e in ipairs(entities) do
    if e.ingame then
      if not e.initiative then
        return false
      end
    end
  end
  return true
end

function engine.pick_enemy_cards()
  -- TODO fix this spaghetti
  for id, e in ipairs(entities) do
    if e.is_enemy and e.is_alive then
      local e_type = e.enemy_type
      local e_id = id
      if not resources.has_enemy_picked_card[e.enemy_type] then
        local cards = {}
        for i, e in ipairs(enemy_abilities[e_type]) do
          if not e.discarded then
            table.insert(cards, i)
          end
        end
        resources.has_enemy_picked_card[e_type] = table.remove(cards, love.math.random(#cards))
        entities[e_id].initiative = enemy_abilities[e_type][resources.has_enemy_picked_card[e_type]].initiative
        if entities[e_id].elite then
          entities[e_id].initiative = entities[e_id].initiative + entities[e_id].number/1000
        else
          entities[e_id].initiative = entities[e_id].initiative + entities[e_id].number/100
        end
      else
        entities[e_id].initiative = enemy_abilities[e_type][resources.has_enemy_picked_card[e_type]].initiative
        if entities[e_id].elite then
          entities[e_id].initiative = entities[e_id].initiative + entities[e_id].number/1000
        else
          entities[e_id].initiative = entities[e_id].initiative + entities[e_id].number/100
        end
      end
    end
  end
  pretty.print(entities)
  resources.has_selected_enemy_cards = true
end

function engine.find_lowest_initiative_char()
  local min = 100
  local index = 0
  for i, e in pairs(entities) do
    if e.initiative and e.ingame and not e.done then
      if e.initiative < min then
        min = e.initiative
        index = i
      end
    end
  end
  return index
end


function engine.execute_card(card)
  resources.current_action = coroutine.create(card)
  local continue = coroutine.resume(resources.current_action)
  while continue do
    continue = coroutine.resume(resources.current_action)
  end
  resources.current_action = nil
end

return engine
