local chapters = {}

chapters[1] = {
  grid = [[
                        3l  3.  3a  3a  3.  3c
                          3c  3.  3.  3.  3c
                        3c  3.  3n  3n  3.  3c
                          3o  3o  3.  3o  3o
1.  1.  1.  1.  1.      3.  3.  3.  3.  3.  3.
  1e  1.  1.  1.                  23
1e  1e  1.  1.  1g  2.  2.  2b  2t  2t  2.  2.  2.
  1E  1.  1.  1G  12  2.  2.  2.  2.  2.  2.  2A
1e  1e  1.  1.  1g  2.  2.  2b  2.  2.  2.  2.  2.
  1e  1.  1.  1.       
1.  1.  1.  1.  1.    
  ]],
  has_boss = false,
  first_room = "1",
  enemy_defs = {
    g = "Bandit Guard",
    a = "Bandit Archer",
    n = "Living Bones",
  },
}

return chapters
