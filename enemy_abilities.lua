local enemy_abilities = {
  ["Bandit Guard"] = {
    {
      initiative = 15,
      actions = {
        shield = 1,
        retaliate = 2,
        reshufle = true,
      }
    },
    {
      initiative = 30,
      actions = {
        move = 1,
        attack = {damage = -1, range = 0},
      }
    },
    {
      initiative = 35,
      actions = {
        move = -1,
        attack = {damage = 0, range = 2},
      }
    },
    {
      initiative = 50,
      actions = {
        move = 0,
        attack = {damage = 0, range = 0},
      }
    },
    {
      initiative = 70,
      actions = {
        move = -1,
        attack = {damage = 1, range = 0},
      }
    },
    {
      initiative = 55,
      actions = {
        move = -1,
        attack = {damage = 0, range = 0},
        effect = {"strengthen", "user"},
      }
    },
    {
      initiative = 55,
      actions = {
        shield = 1,
        attack = {damage = 0, range = 0, effect = "poison"},
        reshufle = true,
      }
    },
  }
}

return enemy_abilities
